import pygame
import time
import random

pygame.init()

display_width =800
display_height =600

gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption("A bit Driving")
clock = pygame.time.Clock()

carImg = pygame.image.load("racecar.png")
carImg_x = carImg.get_width()

def things(thing_x, thing_y, thing_w, thing_h, color):
    pygame.draw.rect(gameDisplay, color, [thing_x, thing_y, thing_w, thing_h])


def car(x, y):
    gameDisplay.blit(carImg, (x, y))

def textObjects(text, font):
    textSurface = font.render(text, True, (255, 0, 0))
    return textSurface, textSurface.get_rect()


def message_display(text):
    largeText =pygame.font.Font("freesansbold.ttf",115)
    textSurf, textRect = textObjects(text, largeText)
    textRect.center = ((display_width/2), (display_height/2))
    gameDisplay.blit(textSurf, textRect)
    pygame.display.update()

    time.sleep(2)

    game_loop()

def crashed():
    message_display("You Crashed")


def game_loop():

    x = (display_width * 0.45)
    y = (display_height * 0.8)

    x_change = 0
    thing_start_x = random.randrange(0, display_width)
    thing_start_y = -600
    thing_speed = 7
    thing_width = 100
    thing_height = 100

    gameExit =False

    while not gameExit:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x_change = -5
                if event.key == pygame.K_RIGHT:
                    x_change = +5

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    x_change = 0

        x += x_change
        gameDisplay.fill((255, 255, 255))

        #things(thing_x, thing_y, thing_w, thing_h, color)
        things(thing_start_x, thing_start_y, thing_width, thing_height, (0, 0, 0))
        thing_start_y += thing_speed
        car(x, y)

        if x > display_width - carImg_x or x < 0:
            crashed()

        if thing_start_y > display_height:
            thing_start_y = 0 - thing_height
            thing_start_x = random.randrange(0, display_width)

        pygame.display.update()
        clock.tick(60) # frame per second


game_loop()
pygame.quit()
quit()