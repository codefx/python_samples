import numpy as np

arr0= np.zeros((5,4))  #satır, sutun
print("Array filled by zeros: ")
print(arr0)
print("""****************""")

arr1= np.ones((4,5))
print("Array filled by ones: ")
print(arr1)
print("""****************""")

arrUN= np.empty((4,5))
print("uninitialized array: ")
print(arrUN)
print("""****************""")

arrIM= np.eye(5)
print("Identity matrix ")
print(arrIM)
print("""****************""")

arrEYE= np.eye(5, k=2) #k is starting column
arrEYEN = np.eye(5, k=-2) #k is starting column
print("matrix with ones on other diagonal ")
print(arrEYE)
print("______________")
print(arrEYEN)
print("""****************""")

arrR= np.arange(1, 14, 2)
print("Array generated from 1 to 14 with step 3 ")
print(arrR)
print("""****************""")

arrLS= np.linspace(3, 14, 6) # eş parçalara bölmek için
print("Array that contains 6 numbers distributed from 3 to 14")
print(arrLS)
print("""****************""")

def fon(x, y):
    return x+(y*2)
def fon2(x, y):
    return x*(y+1)

arrDbF= np.fromfunction(fon, (3,5)) #satır, sutun
arrDbF2= np.fromfunction(fon2, (4,3))
print("arrays Defined by Functions (DbF) where x and y  are indices")
print("like: x:0 y:0 x+y=0 => 0, x:1, y:0 x+y=1 => 1") #x down, y right
print("x down, y right")
print(arrDbF)
print("_______________")
print(arrDbF2)
print("""****************""")
