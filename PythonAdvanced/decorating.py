from functools import wraps  #if remove this, new_monitor.__name__'s result wil be "add_wrapping_with_style"


def add_wrapping_with_style(style):
    def add_wrapping(item):
        @wraps(item)
        def wrapped_item():
            return f'a {style} wrapped box of_ {item()}'
        return wrapped_item
    return add_wrapping

@add_wrapping_with_style('beautifully')
@add_wrapping_with_style('considerably')
def new_monitor():
    return 'a new 4K curved monitor'


print(new_monitor())
print(new_monitor.__name__)


