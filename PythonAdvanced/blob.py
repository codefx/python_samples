import random


class Blob:

    def __init__(self, color, x_coordinate, y_coordinate, sz_rnS=4, sz_rnF=8, mv_rS=-1, mv_rF=2):
        self.color = color
        self.x = random.randrange(0, x_coordinate)
        self.y = random.randrange(0, y_coordinate)
        self.size = random.randrange(sz_rnS, sz_rnF)
        self.x_boundary = x_coordinate
        self.y_boundary = y_coordinate
        self.mv_rS = mv_rS  # range start
        self.mv_rF = mv_rF #range finish

    def __repr__(self):
        return f"Blob({self.color},{self.size},{self.x},{self.y})"

    def __str__(self):
        return f"Blob's color: {self.color}, blob's size: {self.size}, Location_x: {self.x}, Location_y: {self.y})"

    def move(self):
        self.move_x = random.randrange(self.mv_rS, self.mv_rF)
        self.move_y = random.randrange(self.mv_rS, self.mv_rF)
        self.x += self.move_x
        self.y += self.move_y

    def check_bounds(self):

        if self.x < 0: self.x = 0
        elif self.x > self.x_boundary: self.x = self.x_boundary

        if self.y < 0: self.y = 0
        elif self.y > self.y_boundary: self.y = self.y_boundary

    def __add__(self, other):
        if other.color == (255, 0, 0):
            self.size -= other.size
            other.size -= self.size
        elif other.color == (0, 255, 0):
            self.size += other.size
            other.size = 0
        elif other.color == (0, 0, 255):
            pass
        else:
            raise Exception("something went wrong")