class Pet:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def talk(self):
        pass #raise NotImplementedError("subclass must implement this abstract method")


class Cat(Pet):

    def __init__(self, name, age):
        super().__init__( name, age)

    def talk(self):
         return "Miyaaw"


class Dog(Pet):

    def __init__(self, name, age):
        super().__init__(name, age)

    def talk(self):
        return "Hawooofff"


def mmain():

    pets =[Cat("Tekir", 3), Dog("Karabaş", 4), Cat("Minik", 2), Pet("Turtle", 5)]

    for pet in pets:
        #says = pet.talk()
        print(f"{pet.name}, {pet.age}, says  {pet.talk()}")


if __name__ == "__main__":
    mmain()
