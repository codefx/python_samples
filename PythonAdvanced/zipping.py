x = [1, 2, 3, 4]
y = ['a', 'b', 'c', 'd']
z = [7, 8, 1, 2]


for a, b, c in zip(x,y,z):
    print(a,b,c)

print(list(zip(x,y,z)))
print("--------------------------")
print(dict(zip(z,y)))
print("--------------------------")
[print(a, b, c) for a, b, c in zip(x, y, z)]
print("--------------------------")
[print(x,y,z) for x, y, z in zip(x, y, z)] # attention local variable not stored, x list not changed but in regular for loop it changes