article_1 = "You will win your life"
article_2 = "He is very clever"
article_3 = "It is exactly your business"
category = "General category"

def getarticles(category, *args): # *args like a list
    print(category)
    for article in args:
        print(article)



getarticles(category, article_1, article_2, article_3)


def site_articles(site_category, **kwargs):  # **kwargs like a dictionary --> KeyWord-Args
    print(site_category)
    for name, article in kwargs.items(): #like dictionary !! ".items()"
        print(name, article)



site_articles(category, article_1 = "You will win your life",
                        article_2 = "He is very clever",
                        article_3 = "It is exactly your business")


def combineArgsKwargs(catname, *args, **kwargs):
    print(catname)
    for arg in args:
        print(arg)
    for name, article in kwargs.items():
        print(name, article)


argtypes = ['A','B','C']

articles = {"article_1": "You will win your life", "article_2" : "He is very clever", "article_3": "It is exactly your business"}


combineArgsKwargs(category, *argtypes, **articles)




