import pygame
from blob import Blob
import numpy as np

STARTING_BLUE_BLOBS = 15
STARTING_RED_BLOBS = 15
STARTING_GREEN_BLOBS = 45

WIDTH = 800
HEIGHT = 600
WHITE = (255, 255, 255)  # R-G-B red green blue
RED = (255, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)

game_display = pygame.display.set_mode((WIDTH, HEIGHT))  #tuple!
pygame.display.set_caption("Blob blob world")
clock = pygame.time.Clock()


def is_touching(b1, b2):
    return np.linalg.norm(np.array([b1.x, b1.y])-np.array([b2.x, b2.y])) < (b1.size + b2.size)

def handle_collisions(blob_list):
    blues, reds, greens  = blob_list
    for blue_id, blue_blob in blues.copy().items():
        for other_blobs in  blues, reds, greens:
            for other_blob_id, other_blob in other_blobs.copy().items():
                if blue_blob == other_blob:
                    pass
                else:
                    if is_touching(blue_blob, other_blob):
                        blue_blob + other_blob
                        if other_blob.size <= 0:
                            del other_blobs[other_blob_id]
                        if blue_blob.size <= 0:
                            del blues[blue_id]
    return blues, reds, greens




def draw_environment(blob_list):
    game_display.fill(WHITE)
    blues, reds, greens = handle_collisions(blob_list)
    for blob_dict in blob_list:
        for blob_id in blob_dict:
            blob = blob_dict[blob_id]
            pygame.draw.circle(game_display, blob.color, [blob.x, blob.y], blob.size)
            #pygame.draw.rect(game_display,blob.color, (blob.x, blob.y,blob.size,blob.size ), blob.size)

            blob.move()
            blob.check_bounds()


    pygame.display.update()
    return blues, reds, greens


def main():

    blue_blobs = dict(enumerate([Blob(BLUE, WIDTH, HEIGHT) for i in range(STARTING_BLUE_BLOBS)]))
    red_blobs = dict(enumerate([Blob(RED, WIDTH, HEIGHT) for i in range(STARTING_RED_BLOBS)]))
    green_blobs = dict(enumerate([Blob(GREEN, WIDTH, HEIGHT) for i in range(STARTING_GREEN_BLOBS)]))
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

        blue_blobs, red_blobs, green_blobs = draw_environment([blue_blobs, red_blobs, green_blobs])
        clock.tick(60)  #frame per second
        #print(red_blob.x, red_blob.y)


if __name__ == '__main__':
    main()
