from threading import Thread as thr
import time

def timer(name, delay, repeat):
    print(f"Timer {name} started")
    while repeat>0:
        time.sleep(delay)
        print(f"{name} : {time.ctime(time.time())}")
        repeat -=1
    print(f"Timer {name} completed")

def Main():
    th1 = thr(target=timer, args=("Timer-1", 1, 5))
    th2 = thr(target=timer, args=("Timer-2", 2, 5))
    th1.start()
    th2.start()

    print("Main completed")

if __name__ == "__main__":
    Main()