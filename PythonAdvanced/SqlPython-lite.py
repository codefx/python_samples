import sqlite3

def main():
    try:
        con = sqlite3.connect("test.db")
        cur = con.cursor()
        cur.executescript("""Drop Table if exists Pets;
                            Create Table Pets (Id Int, Name Text, Price Int);
                            Insert into Pets Values(1,"Cat",200);
                            Insert into Pets Values(2,"Bird",150);""")
        pets =((3,"Dog",300),
               (4, "Frog", 400),
               (5, "Turtle", 600))
        cur.executemany("Insert into Pets Values(?,?,?)",pets)

        con.commit()

        cur.execute('select * from Pets')
        data = cur.fetchall()

        for row in data:
            print(row)

    except sqlite3.Error:
        if con:
            print("Error, Rolling back!")
            con.rollback()
    finally:
        if con:
            con.close()


if __name__ == "__main__":
    main()