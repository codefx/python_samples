import re
import argparse

def Main():
    parser = argparse.ArgumentParser()
    parser.add_argument("word", help="specify word to search for")
    parser.add_argument("fname", help="specify filename to search ")
    args = parser.parse_args()

    searchFile = open(args.fname)
    lineNum = 0

    for line in searchFile.readlines():
        line = line.strip('\n\r')
        lineNum += 1
        searchResult = re.search(args.word, line, re.M | re.I) # I to ignore case sensitive
        if searchResult:
            print(f"{lineNum} : {line}")

    # for example to run =>> python3 RegularExpressions.py main RegularExpressions.py


if __name__ == "__main__":
    Main()