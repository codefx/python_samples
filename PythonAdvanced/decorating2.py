def addTags(*tags):
    def decorator(toWrapfunc):
        def inside(*args, **kwargs):
            code = toWrapfunc(*args, **kwargs)
            for tag in reversed(tags):
                code = f"<{tag}>{code}</{tag}>"
            return code
        return inside
    return decorator



@addTags("p", "i", "b")
def welcome(name):
    return f"Welcome {name} to Our WebSite"


print(welcome("Codefx"))
