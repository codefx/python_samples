# import multiprocessing
#
# def spawn(num, num2):
#     print(f"Spawned : {num} - {num2}")
#
#
# if __name__ == '__main__':
#     for i in range(500):
#         p =multiprocessing.Process(target=spawn,args=(i, i+1))
#         p.start()
#         p.join()

from multiprocessing import Pool

def job(num):
    return num * 2


if __name__ == '__main__':
    p = Pool(processes=20)
    data = p.map(job, range(20))
    data2 = p.map(job, [7,8,5])
    p.close()
    print(data)
    print(data2)