from string import Template

class MyTemplate(Template):
    delimiter = '#'

def Main():
    cart = []
    cart.append(dict(item="Ayran", price=5, qty=6))
    cart.append(dict(item="Pide", price=14, qty=2))
    cart.append(dict(item="salata", price=6, qty=2))

    t = MyTemplate("#qty x #item =#price")
    total = 0
    print("Cart : ")
    for data in cart:
        print(t.substitute(data))
        total += data["price"]*data["qty"]
    print("Total : "+str(total))
    print("-----------------------") ### my Solution :))---
    for data in cart:
        print(f"{data['qty']} x {data['item']} = {data['qty'] * data['price']}")
    print("Total : " + str(total))




if __name__ == "__main__":
    Main()