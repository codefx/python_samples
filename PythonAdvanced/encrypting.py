#pycrypto package no longer maintained. however pycryptodome as same as An almost drop-in replacement for the old PyCrypto library
#  => pip install pycryptodome
# if you needed  pip install pycryptodome-test-vectors
#for detailed information: https://www.pycryptodome.org/src/installation

import os
from Crypto.Cipher import AES  # Advanced Encryption Standard
from Crypto.Hash import SHA256  #produces a 16 Byte output and great with AES-256
from Crypto import Random


def encrypt(key, filename):
    chunksize = 64*1024
    outputFile = f"(encrypted){filename}"
    fileSize = str(os.path.getsize(filename)).zfill(16)
    IV = Random.new().read(16)  # Initialization Vector

    encryptor = AES.new(key, AES.MODE_CBC, IV)

    with open(filename, "rb") as infile: #rb read binary
        with open(outputFile, "wb") as outFile: #wb write binary
            outFile.write(fileSize.encode("utf-8"))
            outFile.write(IV)

            while True:
                chunk = infile.read(chunksize)

                if len(chunk) == 0:
                    break
                elif len(chunk) % 16 !=0:
                    chunk += b' ' *(16 - (len(chunk) % 16))  # b' ' byte form of string characters

                outFile.write(encryptor.encrypt(chunk))


def decrypt(key, filename):
    chunksize = 64*1024
    outputFile = filename[11:]  #striped "(encrypted)"

    with open(filename, "rb") as infile: #rb read binary
        fileSize = int(infile.read(16))
        IV = infile.read(16)

        decryptor = AES.new(key, AES.MODE_CBC, IV)

        with open(outputFile, "wb") as outFile:
            while True:
                chunk = infile.read(chunksize)

                if  len(chunk) == 0:
                    break
                outFile.write(decryptor.decrypt(chunk))
            outFile.truncate(fileSize)


def getKey(password):
    hasher = SHA256.new(password.encode("utf-8"))
    return hasher.digest()


def main():
    choice = input("Would you like to (E)ncrypt or (D)ecrypt? : ")

    if choice == 'E':
        filename= input("File to encrypt : ")
        password = input("Password : ")
        encrypt(getKey(password), filename)
        print("done")
    elif choice == 'D':
        filename = input("File to decrypt : ")
        password = input("Password : ")
        decrypt(getKey(password), filename)
        print("done")
    else:
        print("No option selected, closing...")
        quit()


if __name__ == "__main__":
    main()


