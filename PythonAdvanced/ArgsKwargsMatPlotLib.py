import matplotlib.pyplot as plt

def graph_operation(x, y):
    print(f"function that graphs {x} and {y}")
    plt.plot(x, y)
    plt.show()


x1 = [1, 2, 3]
y1 = [2, 3, 1]

graph_it = [x1, y1]

graph_operation(x1, y1)

graph_operation(*graph_it)

