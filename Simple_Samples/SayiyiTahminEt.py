import random as rnd

print("Sayı tahmin/ guessnumber oyununa hoş geldiniz tavan/max sayıyı belirleyip başlayabilirsiniz, \n Çıkmak için Q veya q ya ve enter a basınız")
while True:
    try:
        maxnum = input("lütfen tavan sayıyı giriniz \n")
        if maxnum == 'Q' or maxnum == 'q':
            quit()
        else:
            n = rnd.randint(1, int(maxnum))
            print(f"1 ile {maxnum} arasında bir tahmin yapınız !")
            guess = None
            counter = 0
            while guess != n:
                try:
                    guess = int(input("Your try :\n "))
                    counter += 1
                    if guess > n:
                        print("Lower/ daha düşük olmalı")
                    if guess < n:
                        print("Higher / daha yüksek olmalı")
                except ValueError:
                    print("lütfen verilen aralıkta bir SAYI giriniz..!")
            print(f"tebrikler sayıyı {counter} adımda buldunuz")
    except ValueError:
        print("lütfen tam sayı olarak sayı giriniz..!")

