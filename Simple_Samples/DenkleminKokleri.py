import cmath

def solveQuadEq(a, b, c):
    Disc=b**2-4*a*c
    x1=(-b-cmath.sqrt(Disc))/2*a
    x2=(-b+cmath.sqrt(Disc))/2*a
    return x1, x2
print("Welcome to the program")
print(" 'ax2 + bx+ c' şeklindeki denklemin köklerini bulmak için: ")
a=int(input("a nın değerini giriniz : "))
b=int(input("b nin değerini giriniz : "))
c=int(input("c nin değerini giriniz : "))

print(f"Equation/denklem şu şekilde: {a}*x**2+ {b}*x+ {c}=0")
solutions=solveQuadEq(a,b,c)
print("Denklemin kökleri/ roots :")
print(f"x1= {solutions[0]}")
print(f"x2= {solutions[1]}")